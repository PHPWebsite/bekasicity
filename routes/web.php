<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/

Route::get('/', function () {
    return view('home');
});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::resource('posts', 'PostController');
Route::post('posts/{id}', array('uses' => 'PostController@simpan', 'PostController@reply_simpan'));
Route::get('posts/{id}', array('uses' => 'PostController@reply_simpan'));
Route::get('posts/{id}/{post_id?}', array('uses' => 'PostController@show'));
Route::resource('populer', 'HomeController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

Auth::routes();

Route::get('/dashboard', 'DashboardController@populer');

