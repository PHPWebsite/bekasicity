-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2018 at 01:02 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `programmer`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `commentary` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `commentary`, `post_id`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'ssj', 10, 1, '2018-09-12 15:35:03', '2018-09-12 15:35:03'),
(7, 'jjj', 9, 1, '2018-09-12 15:38:52', '2018-09-12 15:38:52'),
(9, 'ayam', 9, 1, '2018-09-14 15:28:47', '2018-09-14 15:28:47'),
(10, 'iya benar sangat bau di daerah bantar gebang', 8, 1, '2018-09-15 06:51:51', '2018-09-15 06:51:51'),
(13, 'tolong diperbaiki', 10, 1, '2018-09-15 11:28:07', '2018-09-15 11:28:07'),
(14, 'bantar gebang merupakan tempat pembuangan sampah', 8, 1, '2018-09-15 11:28:41', '2018-09-15 11:28:41'),
(15, 'Banyak terjadi kecelakaan gara2 jalan berlubang', 9, 2, '2018-09-16 02:55:19', '2018-09-16 02:55:19'),
(16, 'ddd', 10, 2, '2018-09-16 03:04:59', '2018-09-16 03:04:59'),
(17, 'nnn', 10, 2, '2018-09-16 03:11:16', '2018-09-16 03:11:16'),
(18, 'sss', 11, 2, '2018-09-16 03:12:23', '2018-09-16 03:12:23'),
(19, 'iya tuh benarin', 8, 2, '2018-09-16 03:26:04', '2018-09-16 03:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_05_015718_create_posts_table', 1),
(4, '2018_09_05_031222_add_category_to_posts', 2),
(5, '2018_09_09_063346_add_views_to_posts', 3),
(6, '2018_09_11_155739_create_comment_table', 4),
(7, '2018_09_11_161506_create_comments_table', 5),
(8, '2018_09_15_165708_create_reply_table', 5),
(9, '2018_09_15_170322_create_replies_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `image`, `user_id`, `created_at`, `updated_at`, `views`) VALUES
(8, 'Masalah Lingkungan (polusi bau)', '<p>Di daerah<strong> bantar gebang,</strong> polusi bau menyebar sampe radius 3 kilometer menuju perumahan. Ditambah lagi dengan adanya cairan limbah dari truk sampah membuat jalanan menjadi licin dan bau. Mohon solusinya.</p>', '20160624-bantar-gebang-sampah_20160624_055820_1536428299.jpg', 1, '2018-09-08 10:38:19', '2018-09-16 03:32:18', 43),
(9, 'Jalan berlubang', '<p>Di daerah <strong>rawa panjang</strong>, banyak jalan yang berlubang. tolong diperbaiki</p>', 'jalan-berlubang-di-jalan-barito_20170304_150807_1536428428.jpg', 1, '2018-09-08 10:40:28', '2018-09-22 04:24:23', 83),
(10, 'Keamanan', '<p>Banyak terjadi penjambretan dijalan meruya</p>', 'noimage.jpg', 1, '2018-09-08 23:30:49', '2018-09-16 03:48:40', 179),
(11, 'Daerah Rawan', '<p>Terjadi banyak kasus pembegalan jam 8 malam ke atas di <strong>bawah flyover summarecon</strong></p>', 'BHvTIZ1CEAA7GkI_1537066451.jpg', 2, '2018-09-16 02:54:11', '2018-09-22 11:00:47', 28);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'steven roy', 'stevenroy155@gmail.com', '$2y$10$DzU1dGUqXQn5sWwJ4TN23ud3qg6Gar8L9aHmL7vHuJSB0RIiUW38O', 'OTWAGY4LIOr2nMZSBTxHA6uezd6FlZRUrmQWjttWSrKuFwKbyC66af0Xwf3C', '2018-09-08 07:38:04', '2018-09-08 07:38:04'),
(2, 'goodfoot', 'goodfoot@hotmail.com', '$2y$10$qGHaZCf004Wl08Rt5bWrqe2va/ZCdTvjcEIPhYrHcO075MGBDXf1e', 'oWlbDv0jhyCW98CZ2a6wwGa3pVe1OM6tFwWYTxToCEs2Am9IzSlx1H4tAJ9b', '2018-09-16 02:51:55', '2018-09-16 02:51:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
