<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\Comment;
use App\Reply;
use DB;

class HomeController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('views', 'desc')->paginate(5);
        return view('populer.index')->with('posts', $posts);
    }

       public function display()
    {
        $comments = Comment::orderBy('created_at', 'asc');
        return view('posts.show')->with('comments', $comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'image' => 'image|nullable|max:1999'
        ]);

       if($request->hasFile('image')){
            //Get file name extension --> jika mempunyai gambar/file
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get just filename                       
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore); 
        } else {
            $fileNameToStore = 'noimage.jpg'; //jika tidak mengupload file maka akan melihat file default
        }

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->image = $fileNameToStore;
        $post->user_id = auth()->user()->id;
        $post->views = 0;
        $post->save();

        return redirect('/dashboard')->with('success', 'Post Created');
      
    }

     public function simpan($id, Request $request)
    {
        $comment = new Comment;
        $comment->commentary = $request->input('commentary');
        $comment->post_id = $request->route('id');
        $comment->id_user = auth()->user()->id;
        $comment->save();
        return back()->with('success', 'Comment Created');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $post->increment('views');
        $comments = Comment::where('post_id', $id)->orderBy('created_at', 'asc')->get();
        $count= Comment::where('post_id', $id)->count();
        return view('posts.show', compact('post', 'comments', 'count'));
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if(auth()->user()->id != $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
                'title' => 'required',
                'body' => 'required'
        ]);

          if($request->hasFile('image')){
            //Get file name extension --> jika mempunyai gambar/file
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore); 
        } 


        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        if($request->hasFile('image')){
            $post->image = $fileNameToStore;
        }
        $post->save();

        return redirect('/posts')->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = Post::find($id);
        //Check for correct user
        if(auth()->user()->id != $post->user_id){
           return redirect('/populer')->with('error', 'Unauthorized Page'); 
    }

    if($post->image != 'noimage.jpg'){
        Storage::delete('public/images/' .$post->image);
    }

      $post->delete();
        return redirect('/posts')->with('success', 'Post Removed');
    }

}
