<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class DashboardController extends Controller
{
	/**
	*
	*
    * @return void
    */

    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
	*
	*
    * @return \Illuminate\Http\Response
    */

    public function index() 
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
    	return view('dashboard')->with('posts', $user->posts);
    }

     public function populer() 
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('dashboard')->with('posts', $user->posts);
    }


}
