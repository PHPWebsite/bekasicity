<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class logout extends Controller
{
   public function logout () {
 
    auth()->logout();
    // redirect to homepage
    return redirect('/posts');
}
}
