@extends('layouts.app')

@section('content')
		<button class="btn btn-primary pull-right" type="button"> Comments <span class="badge">{{ $count }}</span>
		</button>
		<a href="/posts" class="btn btn-default">Go Back</a>
		<h1>{{ $post->title }}</h1>
		<div>
			{!! $post->body !!}
		</div>
		<br><br>
		<img style = "width:50%" src="/storage/images/{{ $post->image }}" class="center">
		
		
		<hr>
		<small>Written on {{$post->created_at}} by {{$post->user['name']}}</small>
		<hr>
		@if(!Auth::guest())
			@if(Auth::user()->id == $post->user_id)
				<a href="/posts/{{ $post->id }}/edit" class="btn btn-default" >Edit</a>

				{!! Form::open(['action' => ['PostController@destroy', $post->id], 'method'=>'post', 'class' => 'pull-right']) !!}
				{!! Form::hidden('_method', 'DELETE') !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				{!! Form::close() !!}
			@endif

		<hr>
		<br>


		{!! Form::open(['action' => ['PostController@simpan', $post->id] , 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::textarea('commentary', '', ['type' => 'hidden', 'class' => 'form-control form-control-sm', 'placeholder' => 'Add your commmentar..', 'cols' => '30', 'rows' => '7'])}}
        </div>
        {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
		{!! Form::close() !!}
		<br>

		@endif

		

		@if(count($comments) > 0)

			@foreach($comments as $comment)

				<div class="panel panel-default">

					<div class="panel-heading">
						<strong>{!! $comment->user['name'] !!}</strong>
					</div>

					<div class="panel-body">
						{!! $comment->commentary !!}
					</div>

				</div>
		
			@endforeach

		@endif
	
@endsection